<!DOCTYPE html>
<html lang="ja">
<head>
  <meta charset="UTF-8">
<meta name="viewport" content="width=device-width,initial-scale=1,user-scalable=yes;">
  <title>calendar</title>
<?php
ini_set("display_errros","1");
date_default_timezone_set('Asia/Tokyo');
$year = 0; //表示する年
$month = 0; // 表示する月
$dow = array('日','月','火','水','木','金','土');

if (isset($_GET['y'])){
  $year = intval($_GET['y']);
}

if (isset($_GET['m'])){
  $month = intval($_GET['m']);
}

if ( !$year || !$month || !checkdate( $month, 1, $year )){
  // 現在の年月
  $now_date = new DateTime();
  $year = $now_date->format('Y');
  $month = $now_date->format('m');
}

try {
  $datetime = new DateTime("{$year}-{$month}-1"); // 表示する月
} catch (Exception $e){
  echo $e->getMessage();
  exit(1);
}

$first_day = $datetime->format('w'); // 1日目の曜日 
$last_day = new DateTime("{$datetime->format('Y')}-{$datetime->format('m')}-{$datetime->format('t')}");
?>
</head>
<body>
<table border="1">
<caption><?= $datetime->format('Y年m月')?></caption>
  <thead>
    <tr>
    <th colspan="3">
<?php
$datetime->modify('-1 month');
?>
  <a href="/calendar/?y=<?= $datetime->format('Y') ?>&m=<?= $datetime->format('m') ?>">前月</a></th>
<th> </th>
    <th colspan="3">
<?php
// 前月のところで一つ減っているので＋２になっている。
$datetime->modify('+2 months');
?>
<a href="/calendar/?y=<?= $datetime->format('Y') ?>&m=<?= $datetime->format('m') ?>">次月</a></th>
    </tr>
    <tr>
<?php foreach ($dow as $day): ?>
<th><?= $day ?></th>
<?php endforeach; ?>
    </tr>
  </thead>
  <tbody>
<tr>
<?php
// 前月、次月ナビゲーションでずれた月数を元に戻す
$datetime->modify('-1 month');
for($date = 1;$date <= $datetime->format('t');$date++){ ?>
<?php // 1日が日曜日じゃない場合、該当曜日直前まで空白セルを表示する。
$i = 0;
if($date === 1) {
  while($i < $first_day ){ ?>
<td> </td>
<?php 
    $i++;
  } // while
} // if    
?>
  <td><?= $date ?>
<?php
if($date === intval($last_day->format('t'))){
  $j = $last_day->format('w');
  while($j < 6){ ?>
<td> </td>
<?php 
    $j++;
  }
}
?>
</td>
<?php if($date === intval($last_day->format('t')) && intval($last_day->format('w')) === 6){ ?>
</tr>
<?php  } elseif($date % 7 === 7 - $first_day){ ?>
</tr><tr>
<?php } elseif($date % 7 === 0 && intval($datetime->format('w')) === 0){?>
</tr><tr>
<?php } // if ?>
<?php } //for ?>
</tr>
</tbody>
</table>
</body>
</html>


